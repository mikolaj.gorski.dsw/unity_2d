﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider2D))]

public class Item : MonoBehaviour
{
    //colider trigger = zrobione (requireComponent/GetComponent)
    //interaction type

    public enum InteractionType { NONE,PickUp,Examine }
    public InteractionType type;

    [Header("Read")]
    public string descriptionText;

    private void Reset()
    {
        GetComponent<Collider2D>().isTrigger = true;
        gameObject.layer = 11;
    }

    public void Interact()
    {
        switch(type)
        {
            case InteractionType.PickUp:
                //Add the object to the PickUpItems list
                FindObjectOfType<Interaction_system>().PickUpItem(gameObject);
                FindObjectOfType<Interaction_system>().ReadItem(this);

                //delete the object: Destroy(gameObject);

                //disable object
                gameObject.SetActive(false);
                Debug.Log("Item Picked Up");
                break;
            case InteractionType.Examine:
                FindObjectOfType<Interaction_system>().ReadItem(this);
                //gameObject.SetActive(false);
                Debug.Log("Examine");
                
                break;
            default:
                Debug.Log("NULL ITEM");
                break;
        }
    }

}

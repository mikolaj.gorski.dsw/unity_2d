﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_movement : MonoBehaviour
{
    private BoxCollider2D boxCollider;
    private Vector3 moveDelta;
    private RaycastHit2D hit;
    public Animator animator;


    private void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    private void FixedUpdate()
    {
        if (CanMove()==false)
            return;

        
        
       float x = Input.GetAxisRaw("Horizontal");
       float y = Input.GetAxisRaw("Vertical");

       animator.SetFloat("Speed_side", Mathf.Abs(x));
       animator.SetFloat("Speed", Mathf.Abs(y));

        //Reset MoveDelta
        moveDelta = new Vector3(x,y,0);
        //Swap sprite directions
        if (moveDelta.x > 0)
            transform.localScale = Vector3.one;
        else if(moveDelta.x < 0)
            transform.localScale = new Vector3(-1, 1, 1);

        //make sure we can move this direction~, if box return null we can go there ;)
        hit = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, moveDelta.y), Mathf.Abs(moveDelta.y * Time.deltaTime), LayerMask.GetMask("Actor", "Blocking"));
            if(hit.collider == null)
        {
            //move player
            transform.Translate(0, moveDelta.y * 0.65f * Time.deltaTime, 0);
        }

        hit = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(moveDelta.x, 0), Mathf.Abs(moveDelta.x * Time.deltaTime), LayerMask.GetMask("Actor", "Blocking"));
            if (hit.collider == null)
        {
            //move player 
            transform.Translate(moveDelta.x * 0.65f * Time.deltaTime, 0, 0);
        }

            // czy może się poruszać podczas czytanie (isReading)
        bool CanMove()
        {
            bool can = true;
            if (FindObjectOfType<Interaction_system>().isReading)
                can = false;
            return can;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interaction_system : MonoBehaviour
{
    [Header("Detetion Parameters")]
    //Detection Point
    public Transform detectionPoint;
   //detection radius
    private const float detectionRadius = 0.02f;
    //detection layer
    public LayerMask detectionLayer;
    //cashed Trigger Object
    public GameObject detectedObject;

    [Header("Read Parameters")]
    //read window
    public GameObject readWindow;
    public Image readImage;
    public Text readText;
    public bool isReading;
    public GameObject noteDescription;

    [Header("Others")]
    //list of picked items
    public List<GameObject> pickedItems = new List<GameObject>();

    void Update()
    {
        
        if (DetectObject())
        {
            if(InteractInput())
            {
                detectedObject.GetComponent<Item>().Interact();
            }
        }
    }


    bool InteractInput()
    {
        return Input.GetKeyDown(KeyCode.F);

    }

    bool DetectObject()
    {

        
        Collider2D obj = Physics2D.OverlapCircle(detectionPoint.position,detectionRadius,detectionLayer);
        
        if (obj == null)
        {
            detectedObject = null;
            return false;
        }
        else
        {
            detectedObject = obj.gameObject;
            return true;
        }
            
    }

    public void PickUpItem(GameObject item)
    {
        pickedItems.Add(item);

        if (isReading)
        {
            Debug.Log("CLOSE");
            //hide the reading window
            readWindow.SetActive(false);
            //disable boolean
            isReading = false;
            //noteDescription.GetComponent<Item>().descriptionText = item.description;
            //noteDescription.GetComponent<Item>().descriptionText = item.descriptionText;
            
        }
        else
        {

            Debug.Log("READING");
            //readText.text = item.descriptionText;
            readWindow.SetActive(true);
            isReading = true;
            

        }
        
    }

    public void ReadItem(Item item)
    {

        if (isReading)
        {
            Debug.Log("CLOSE");
            //hide the reading window
           // readWindow.SetActive(false);
           // //disable boolean
            //isReading = false;
        }
        else
        {
           // Debug.Log("READING");
            //readText.text = item.descriptionText;
            
            readWindow.SetActive(true);
           // isReading = true;
        }
        //Show image in the middle
        //readImage.sprite = item.GetComponent<SpriteRenderer>().sprite;
        //Write description in the center of the image 
        
      
    }

}
